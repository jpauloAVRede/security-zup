package br.com.zup.Security.perfil;

import br.com.zup.Security.perfil.dtos.PostagemDTO;
import br.com.zup.Security.perfil.dtos.UsuarioDTO;
import br.com.zup.Security.post.Postagem;
import br.com.zup.Security.post.PostagemRepository;
import br.com.zup.Security.user.Usuario;
import br.com.zup.Security.user.UsuarioRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/perfil")
public class PerfilController {
    @Autowired
    private PostagemRepository postagemRepository;
    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private ObjectMapper conversor;

    @GetMapping()
    public UsuarioDTO exibirTodasPostagem(Authentication authentication){
        Usuario usuario = usuarioRepository.findByEmail(authentication.getName()).get();
        List<Postagem> postagens = postagemRepository.findAllByUsuarioEmail(authentication.getName());

        List<PostagemDTO> postagemDTOS = postagens.stream()
                .map(postagem -> conversor.convertValue(postagem, PostagemDTO.class)).collect(Collectors.toList());

        UsuarioDTO usuarioDTO = conversor.convertValue(usuario, UsuarioDTO.class);
        usuarioDTO.setPostagens(postagemDTOS);

        return usuarioDTO;
    }

}
