package br.com.zup.Security.jwt;

import br.com.zup.Security.exceptions.TokenNotValidException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JWTComponente {
    //chave para fazer a criptografia e a descriptografia das coisas
    @Value("${jwt.chave}") //*** falta implementar a variavel de ambiente
    private String chave;
    //tempo de expiração do token - é contato em milissegundos
    @Value("${jwt.milissegundos}") //utilizando variavel dinamica que vem do properties
    private Long milissegundos;

    //metodo para gerar o token
    public String gerarToken(String userName, int idUsuario){
        //pegando o tempo atual + milissegundos do atributo
        Date vencimento = new Date(System.currentTimeMillis() + milissegundos);

        String token = Jwts.builder().setSubject(userName)
                .claim("idUsuario", idUsuario) //adicionando as informações do usuario
                .setExpiration(vencimento) //quando vai expirar
                .signWith(SignatureAlgorithm.HS512, chave.getBytes()) //qual assinatura tá utilizando
                .compact();

        return token;
    }

    //claims são todas as informações de um token
    //metodo para converter token em claims
    public Claims getClaims(String token){
        try {
            Claims claims = Jwts.parser().setSigningKey(chave.getBytes()).parseClaimsJws(token).getBody();
            return claims;
        }catch (Exception exception){
            throw new TokenNotValidException(); //posso criar um exception para o TokenNaoValido
        }
    }

    //metodo para verificar se o token é valido ou não
    public boolean isTokenValid(String token){
        try{
            Claims claims = getClaims(token);
            String username = claims.getSubject();
            Date vencimento = claims.getExpiration();
            Date dataAtual = new Date(System.currentTimeMillis());

            if (username != null && vencimento != null && dataAtual.before(vencimento)){
                return true;
            }else{
                return false;
            }
        }catch (TokenNotValidException exception){
            return false;
        }
    }

}
