package br.com.zup.Security.jwt;

import br.com.zup.Security.exceptions.AcessoNegadoException;
import br.com.zup.Security.jwt.dtos.LoginDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class FiltroDeAutenticacaoJWT extends UsernamePasswordAuthenticationFilter {
    //gera o token - que vem da classe criada
    private JWTComponente jwtComponente;
    private AuthenticationManager authenticationManager;

    public FiltroDeAutenticacaoJWT(AuthenticationManager authenticationManager, JWTComponente jwtComponente) {
        this.authenticationManager = authenticationManager;
        this.jwtComponente = jwtComponente;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        ObjectMapper objectMapper = new ObjectMapper();
        //verificar se existe o usuario no banco de dados
        try {
            //convertendo a resquest em LoginDTO
            LoginDTO loginDTO = objectMapper.readValue(request.getInputStream(), LoginDTO.class);
            //gerencia a autenticação do token
            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                    loginDTO.getEmail(), loginDTO.getSenha(), new ArrayList<>() //essa lista são as permissões
            );

            Authentication auth = authenticationManager.authenticate(authToken);
            return auth;

        }catch (IOException exception){
            throw new AcessoNegadoException();
        }
    }

    //se existe o usuario - gera o token -
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        UsuarioLogin usuarioLogin = (UsuarioLogin) authResult.getPrincipal();
        String username = usuarioLogin.getUsername();
        int idUsuario = usuarioLogin.getId();

        String token = jwtComponente.gerarToken(username , idUsuario);
        response.setHeader("Access-Control-Expose-Headers", "Authorization");
        response.addHeader("Authorization", "Token "+token);
    }
}
