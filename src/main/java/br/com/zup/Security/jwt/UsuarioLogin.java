package br.com.zup.Security.jwt;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class UsuarioLogin implements UserDetails {
    private int id;
    private String email;
    private String senha;

    public UsuarioLogin(int id, String email, String senha) {
        this.id = id;
        this.email = email;
        this.senha = senha;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
//Metodos gerados do implements

    //são as autorizações que o usuário terão
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return this.senha;
    }

    @Override
    public String getUsername() {
        return this.email;
    }

//serie de afirmativas para bloquear o usuario
    //essa conta não está expirada
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    //não estar trancada
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    //as credenciais não estão expiradas
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    //está ativa
    @Override
    public boolean isEnabled() {
        return true;
    }

}
