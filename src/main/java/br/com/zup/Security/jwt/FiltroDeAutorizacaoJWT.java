package br.com.zup.Security.jwt;

import br.com.zup.Security.exceptions.TokenNotValidException;
import io.jsonwebtoken.Claims;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FiltroDeAutorizacaoJWT extends BasicAuthenticationFilter {
    private JWTComponente jwtComponente;
    private UserDetailsService userDetailsService;

    public FiltroDeAutorizacaoJWT(AuthenticationManager authenticationManager, JWTComponente jwtComponente, UserDetailsService userDetailsService) {
        super(authenticationManager);
        this.jwtComponente = jwtComponente;
        this.userDetailsService = userDetailsService;
    }

    public UsernamePasswordAuthenticationToken pegarAutenticacao(HttpServletRequest request, String token){
        if (!jwtComponente.isTokenValid(token)){
            throw new TokenNotValidException();
        }

        Claims claims = jwtComponente.getClaims(token);
        //verifica se existe no banco de dados - vem da classe UsuarioLoginService
        UserDetails usuario = userDetailsService.loadUserByUsername(claims.getSubject());
        //null são as credenciais
        return new UsernamePasswordAuthenticationToken(usuario, null, usuario.getAuthorities());
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        //pegando o token no header
        String token = request.getHeader("Authorization");
        //tem que vir com Token+espaço ("Token ")
        if (token != null && token.startsWith("Token ")){
            try {
                UsernamePasswordAuthenticationToken auth = pegarAutenticacao(request, token.substring(6));
                //classe que fica monitorando se deixa passar ou não as requisições
                SecurityContextHolder.getContext().setAuthentication(auth);
            }catch (TokenNotValidException exception){
                System.out.println(exception.getMessage());
            }
        }
        //depois que passar a autenticação, o chain passa a requisição para adiante, seguindo o funcionamento do sistema
        chain.doFilter(request, response);
    }
}
