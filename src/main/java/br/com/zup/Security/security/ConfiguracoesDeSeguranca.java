package br.com.zup.Security.security;

import br.com.zup.Security.jwt.FiltroDeAutenticacaoJWT;
import br.com.zup.Security.jwt.FiltroDeAutorizacaoJWT;
import br.com.zup.Security.jwt.JWTComponente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration //dizer que é uma configuração
@EnableWebSecurity //devo informar que esta classe vai sobrepor aquela que já existe
public class ConfiguracoesDeSeguranca extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private JWTComponente jwtComponente;

    //cria-se constante para deixar o metodo mais simples
    private static final String[] GET_PUPLICOS = {
            "/usuario/{\\d+}"
    };
    private static final String[] POST_PUPLICOS = {
            "/usuario",
            "/login"
    };

    //traremos novas configurações de segurança...
    //pegar as configurações da WebSecurityAda e reconfigura o que existe

    //sobrescreve os metodos
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //desabilitar a segurança csrf, pois não utilizamos templates e não sofremos ataque DDoS
        http.csrf().disable();

        //permite que todos os dominios acessem nossa url - pq a API será publica;
        //utilizando o objeto de configuração de Cors;
        http.cors().configurationSource(configuracaoDeCors());

        //deixar o endereço de cadastro publico - fica aberto para todos
        http.authorizeRequests().antMatchers(HttpMethod.POST, POST_PUPLICOS).permitAll()
                .antMatchers(HttpMethod.GET, GET_PUPLICOS).permitAll()
                .anyRequest().authenticated(); //o restante precisa de autorização para ser utilizado;
        //desabilitar o não guardar status de objetos - apenas atualiza e salva no banco;
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        //filtro de autenticação
        http.addFilter(new FiltroDeAutenticacaoJWT(authenticationManager(), jwtComponente));

        //filtro de autorização
        http.addFilter(new FiltroDeAutorizacaoJWT(authenticationManager(), jwtComponente, userDetailsService));
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }

    @Bean //precisa desse objeto de cors pra quem precissar
    CorsConfigurationSource configuracaoDeCors(){
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        // "/**" permitir a todos acessar
        source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return source;
    }

    @Bean //gerador de cryptografia - deve ser sempre o mesmo, por isso utiliza-se o bean
    BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

}
