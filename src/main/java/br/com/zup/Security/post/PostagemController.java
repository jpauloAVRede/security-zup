package br.com.zup.Security.post;

import br.com.zup.Security.jwt.JWTComponente;
import br.com.zup.Security.post.dtos.PostDTO;
import br.com.zup.Security.post.dtos.PostUsuarioDTO;
import br.com.zup.Security.post.dtos.PostUsuarioEmaolDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/posts")
public class PostagemController {
    @Autowired
    private PostagemService postagemService;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private JWTComponente jwtComponente;
/* //metodo com ID e trabalhando
    @PostMapping()
    public PostDTO cadastrarPostagem(@RequestBody PostDTO postDTO, HttpServletRequest request){
        String token = request.getHeader("Authorization");
        Claims claims = jwtComponente.getClaims(token.substring(6));
        Postagem postagemModel = modelMapper.map(postDTO, Postagem.class);
        postagemModel = this.postagemService.salvarPostagem(postagemModel, (int) claims.get("idUsuario"));
        return modelMapper.map(postagemModel, PostDTO.class);
    }

 */

    //metodo utilizando o email com authentication
    @PostMapping()
    public Postagem cadastrarPostagem(@RequestBody PostDTO postDTO, Authentication authentication) {
        Postagem postagemModel = modelMapper.map(postDTO, Postagem.class);
        String email = authentication.getName();
        return this.postagemService.salvarPostagem(postagemModel, email);
    }

//    @GetMapping("/perfil")
//    public PostUsuarioEmaolDTO exibirTodasPostagem(Authentication authentication){
//        String email = authentication.getName();
//        List<Postagem> postagems = this.postagemService.exibirTodasPostagemUsuarioPorEmail(email);
//        List<PostUsuarioDTO> postUsuarioDTOS = postagems.stream().map(postagem -> modelMapper.map(postagem, PostUsuarioDTO.class)).collect(Collectors.toList());
//        PostUsuarioEmaolDTO postUsuarioEmaolDTO = new PostUsuarioEmaolDTO();
//        postUsuarioEmaolDTO.setNomeUsuario(authentication.getName());
//        postUsuarioEmaolDTO.setPostagensUsuarioEmailDTO(postUsuarioDTOS);
//        return postUsuarioEmaolDTO;
//    }

    @DeleteMapping("/{idPostagem}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarPostagem(@PathVariable int idPostagem, Authentication authentication){
        try {
            this.postagemService.deletarPost(idPostagem, authentication.getName());
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }

    }

}
