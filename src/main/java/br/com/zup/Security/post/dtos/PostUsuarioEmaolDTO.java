package br.com.zup.Security.post.dtos;

import java.util.List;

public class PostUsuarioEmaolDTO {
    private String nomeUsuario;
    private List<PostUsuarioDTO> postagensUsuarioEmailDTO;

    public PostUsuarioEmaolDTO() {
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public List<PostUsuarioDTO> getPostagensUsuarioEmailDTO() {
        return postagensUsuarioEmailDTO;
    }

    public void setPostagensUsuarioEmailDTO(List<PostUsuarioDTO> postagensUsuarioEmailDTO) {
        this.postagensUsuarioEmailDTO = postagensUsuarioEmailDTO;
    }
}
