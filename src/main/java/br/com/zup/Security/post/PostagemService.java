package br.com.zup.Security.post;

import br.com.zup.Security.user.Usuario;
import br.com.zup.Security.user.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class PostagemService {
    @Autowired
    private PostagemRepository postagemRepository;
    @Autowired
    private UsuarioService usuarioService;

    public Postagem salvarPostagem(Postagem postagem, int id){
        Usuario usuario = this.usuarioService.buscarUsuarioPeloId(id);

        postagem.setUsuario(usuario);
        postagem.setData(LocalDate.now());

        return this.postagemRepository.save(postagem);
    }

    public Postagem salvarPostagem(Postagem postagem, String email){
        Usuario usuario = this.usuarioService.buscarUsuarioPeloEmail(email);

        postagem.setUsuario(usuario);
        postagem.setData(LocalDate.now());

        return this.postagemRepository.save(postagem);
    }

    public List<Postagem> exibirTodasPostagemUsuarioId(int idUsuario){
        return this.postagemRepository.findAllByUsuarioId(idUsuario);
    }

    public List<Postagem> exibirTodasPostagemUsuarioPorEmail(String email){
        return this.postagemRepository.findAllByUsuarioEmail(email);
    }

    public void deletarPost(int idMensagem, String emailAutor){
        if (!mensagemDoAutor(idMensagem, emailAutor)){
            throw new RuntimeException("O autor pode apagar apenas as proprias mensagens!!!");
        }
        this.postagemRepository.deleteById(idMensagem);
    }

    //verifca se a mensagem é do autor - impede que outros apaguem posts de outras pessoas
    public boolean mensagemDoAutor(int idMensagem, String emailAutor){
        Optional<Postagem> postagemOptional = postagemRepository.findById(idMensagem);
        postagemOptional.orElseThrow(() -> new RuntimeException("Postagem não encontrada"));

        if (!postagemOptional.get().getUsuario().getEmail().equals(emailAutor)){
            return false;
        }
        return true;
    }

}
