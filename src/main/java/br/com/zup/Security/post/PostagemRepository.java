package br.com.zup.Security.post;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PostagemRepository extends CrudRepository<Postagem, Integer> {

    List<Postagem> findAllByUsuarioId(int idUsuario);

    List<Postagem> findAllByUsuarioEmail(String email);
}
