package br.com.zup.Security.post.dtos;

public class PostDTO {
    private String mensagem;

    public PostDTO() {
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
