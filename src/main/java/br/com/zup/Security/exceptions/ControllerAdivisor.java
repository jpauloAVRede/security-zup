package br.com.zup.Security.exceptions;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;

@RestControllerAdvice
public class ControllerAdivisor {

    @ExceptionHandler(AcessoNegadoException.class)
    public ResponseEntity<?> runtimeHandler(AcessoNegadoException exception){
        HashMap<String, String> mesagem = new HashMap<>();
        mesagem.put("mensagemErro", exception.getMessage());

        return ResponseEntity.status(exception.getStatuscode()).body(mesagem);
    }

    @ExceptionHandler(TokenNotValidException.class)
    public ResponseEntity<?> runtimeHandler(TokenNotValidException exception){
        HashMap<String, String> mesagem = new HashMap<>();
        mesagem.put("mensagemErro", exception.getMessage());

        return ResponseEntity.status(exception.getStatusCode()).body(mesagem);
    }

}
