package br.com.zup.Security.exceptions;

public class AcessoNegadoException extends RuntimeException{
    private int statuscode = 403;

    public AcessoNegadoException(){
        super("Acesso negado");
    }

    public int getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(int statuscode) {
        this.statuscode = statuscode;
    }
}
