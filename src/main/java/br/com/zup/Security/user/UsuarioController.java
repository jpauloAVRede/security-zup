package br.com.zup.Security.user;

import br.com.zup.Security.user.dtos.UsuarioDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {
    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public UsuarioDTO cadstrarUsuario(@RequestBody Usuario usuario){
        Usuario usuarioModel = this.usuarioService.cadastrarUsuario(usuario);
        return modelMapper.map(usuarioModel, UsuarioDTO.class);
    }

    @GetMapping("/{idUsuario}")
    public UsuarioDTO pesquisaUsuarioPeloId(@PathVariable(name = "idUsuario") int id){
        try {
            Usuario usuarioModel = this.usuarioService.buscarUsuarioPeloId(id);
            return modelMapper.map(usuarioModel, UsuarioDTO.class);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
