package br.com.zup.Security.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {
    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public Usuario cadastrarUsuario(Usuario usuario){
        //faço primeiramente a cryptografia antes de salvar a senha;
        String encode = bCryptPasswordEncoder.encode(usuario.getSenha());
        usuario.setSenha(encode);
        return this.usuarioRepository.save(usuario);
    }

    public Usuario buscarUsuarioPeloId(int id){
        Optional<Usuario> usuarioOptional = this.usuarioRepository.findById(id);

        if (usuarioOptional.isEmpty()){
            throw new RuntimeException("Usuario não encontrado");
        }
        return usuarioOptional.get();
    }

    public Usuario buscarUsuarioPeloEmail(String email){
        Optional<Usuario> usuarioOptional = this.usuarioRepository.findByEmail(email);

        if (usuarioOptional.isEmpty()){
            throw new RuntimeException("Usuario não encontrado");
        }
        return usuarioOptional.get();
    }

}
